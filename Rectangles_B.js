const RectangleRelations = Object.freeze({
    INTERSECT: "Rectangles intersect",
    NO_INTERSECTION: "Rectangles do not intersect",
})


/**
 * 
 * @param {<{ x:Number,  y:Number }>[4]} a rectangle can be described by 4 points with x and y coordinates
 * @param {<{ x:Number,  y:Number }>[4]} b 
 * @returns {string}
 */
function rectangles(a, b){

    /**
     * 
     * @param {<{ x:Number,  y:Number }>} p1 start of line 1
     * @param {<{ x:Number,  y:Number }>} p2 end of line 1
     * @param {<{ x:Number,  y:Number }>} p3 start of line 2
     * @param {<{ x:Number,  y:Number }>} p4 end of line 2
     * @returns {Boolean}
     */
    function doLinesIntersect(p1, p2, p3, p4){
        
        // 0 = A*x + B*y +C
        const A1 = p2.y - p1.y,
            B1 = p1.x - p2.x,
            C1 = (p1.y*p2.x) - (p1.x*p2.y),
            A2 = p4.y - p3.y,
            B2 = p3.x - p4.x,
            C2 = (p3.y*p4.x) - (p3.x*p4.y);
        
        /**
         * 
         * @param {Number} a 
         * @param {Number} b 
         * @param {Number} c 
         * @param {<{ x:Number,  y:Number }>} p 
         * @returns {Number}
         */
        const calculateOrientationCoef = (a,b,c,p) => {
            return (a*p.x) + (b*p.y) + c;
        }

        const coef1 = calculateOrientationCoef(A2, B2, C2, p1),
            coef2 = calculateOrientationCoef(A2, B2, C2, p2);
        if((coef1*coef2)>0){ return false; } // both points are on the same side of the line segment, means they do not intersect

        const coef3 = calculateOrientationCoef(A1, B1, C1, p3),
            coef4 = calculateOrientationCoef(A1, B1, C1, p4);
        if((coef3*coef4)>0){ return false; }
        
        const denominator = (A1*B2) - (A2*B1);
        if(!denominator){ // 0 denominator means lines are collinear
            return false;
        }

        return true;
    }

    for(let i=0; i<4; i++){
        const corner1 = a[i],
            corner2 = a[(i+1)%4];
        for(let j=0; j<4; j++){
            const corner3 = b[j],
                corner4 = b[(j+1)%4];
            // if at least one line segment intersects, rectangles intersect 
            if(doLinesIntersect(corner1, corner2, corner3, corner4)){
                return RectangleRelations.INTERSECT;
            }

        }
    }

    return RectangleRelations.NO_INTERSECTION;
}

console.log(rectangles([{x:1,y:6},{x:5,y:6},{x:5,y:1},{x:1,y:1}],[{x:2,y:5},{x:6,y:5},{x:6,y:3},{x:2,y:3}])); // Rectangles intersect
console.log(rectangles([{x:0,y:3},{x:2,y:3},{x:2,y:2},{x:0,y:2}],[{x:0,y:1},{x:2,y:1},{x:2,y:0},{x:0,y:0}])); // Rectangles do not intersect


const RectangleRelations = Object.freeze({
    SAME_RECTANGLE: "It's the same rectangle",
    A_CONTAINS_B: "Rectangle A contains rectangle B",
    B_CONTAINS_A: "Rectangle B contains rectangle A",
    INTERSECT: "Rectangles intersect",
    ADJACENT_CORNER: "Rectangles are adjacent (Corner)",
    ADJACENT_PROPER: "Rectangles are adjacent (Proper)",
    ADJACENT_SUB: "Rectangles are adjacent (Sub-line)",
    ADJACENT_PARTIAL: "Rectangles are adjacent (Partial)",
    NO_RELATION: "No relation",
})


/**
 * Assuming rectangles are paralel
 * 
 * @param {number[4]} a rectangle can be described by 4 coordinates, coordianates of upper left and lover right corners [x1,y1,x2,y2]
 * @param {number[4]} b 
 * @returns {string}
 */
function rectangles(a, b){

    let sides = {
        includes:[],
        same_line:[],
        inside:[],
        touches_out:[],
    };

    for(let i=0; i<4; i++){
        const sideA1 = a[i%2 ? 3 : 0];
        const sideA2 = a[i%2 ? 1 : 2];
        const lowerSide = [0,3].includes(i);

        if((b[i]<sideA1 && lowerSide) || (b[i]>sideA2 && !lowerSide)){ 
            sides.includes.push(i);
        } else if((b[i]===sideA1 && lowerSide) || (b[i]===sideA2 && !lowerSide)){ 
            sides.same_line.push(i);
        } else if( b[i] > sideA1 && b[i] < sideA2){
            sides.inside.push(i);
        } else if ((b[i] === sideA2 && lowerSide) || (b[i] === sideA1 && !lowerSide)){
            sides.touches_out.push(i);
        } else {
            return RectangleRelations.NO_RELATION;
        }
    }

    if(sides.same_line.length === 4) { 
        return RectangleRelations.SAME_RECTANGLE; 
    }
    if((sides.same_line.length + sides.includes.length) === 4) { 
        return RectangleRelations.B_CONTAINS_A; 
    }
    if((sides.same_line.length + sides.inside.length) === 4) { 
        return RectangleRelations.A_CONTAINS_B; 
    }
    if( 
        sides.inside.length >=1
        && sides.includes.length >= 1
        && (sides.same_line.length + sides.inside.length + sides.includes.length) === 4
        ) { 
        return RectangleRelations.INTERSECT; 
    }
    
    if(sides.touches_out.length === 2 && sides.includes.length === 2) { 
        return RectangleRelations.ADJACENT_CORNER; 
    }
    if(sides.touches_out.length === 1 && sides.includes.length === 1 &&  sides.same_line.length === 2) { 
        return RectangleRelations.ADJACENT_PROPER; 
    }
    if(
        (sides.touches_out.length === 1 && sides.inside.length === 2 &&  sides.includes.length === 1)
        || (sides.touches_out.length === 1 && sides.includes.length === 3)
        ) { 
        return RectangleRelations.ADJACENT_SUB; 
    }
    if(sides.touches_out.length === 1 && sides.inside.length === 1 &&  (sides.includes.length + sides.same_line.length) === 2) { 
        return RectangleRelations.ADJACENT_PARTIAL; 
    }

    throw new Error('Invalid Input');
}


console.log([0,4, 2,2],[1,2, 3,0], rectangles([0,4, 2,2],[1,2, 3,0])); // [ 0, 4, 2, 2 ] [ 1, 2, 3, 0 ] Rectangles are adjacent (Partial)
console.log([0,4, 3,2],[1,2, 2,0], rectangles([0,4, 3,2],[1,2, 2,0])); // [ 0, 4, 3, 2 ] [ 1, 2, 2, 0 ] Rectangles are adjacent (Sub-line)
console.log([0,4, 2,2],[0,2, 2,0], rectangles([0,4, 2,2],[0,2, 2,0])); // [ 0, 4, 2, 2 ] [ 0, 2, 2, 0 ] Rectangles are adjacent (Proper)
console.log([0,4, 2,2],[2,2, 4,0], rectangles([0,4, 2,2],[2,2, 4,0])); // [ 0, 4, 2, 2 ] [ 2, 2, 4, 0 ] Rectangles are adjacent (Corner)
console.log([0,4, 2,2],[1,3, 3,0], rectangles([0,4, 2,2],[1,3, 3,0])); // [ 0, 4, 2, 2 ] [ 1, 3, 3, 0 ] Rectangles intersect
console.log([2,3, 3,2],[2,3, 3,2], rectangles([2,3, 3,2],[2,3, 3,2])); // [ 2, 3, 3, 2 ] [ 2, 3, 3, 2 ] It's the same rectangle
console.log([2,3, 3,2],[1,4, 3,0], rectangles([2,3, 3,2],[1,4, 3,0])); // [ 2, 3, 3, 2 ] [ 1, 4, 3, 0 ] Rectangle B contains rectangle A
console.log([1,6, 5,1],[2,3, 3,2], rectangles([1,6, 5,1],[2,3, 3,2])); // [ 1, 6, 5, 1 ] [ 2, 3, 3, 2 ] Rectangle A contains rectangle B
console.log([1,6, 5,1],[20,30, 30,20], rectangles([1,6, 5,1],[20,30, 30,20])); // [ 1, 6, 5, 1 ] [ 20, 30, 30, 20 ] No relation